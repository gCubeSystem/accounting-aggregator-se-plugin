This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Accounting Aggregator Smart Executor Plugin

## [v2.1.0]

- Upgraded smart-executor-bom to 3.3.0 [#27999]
- Updated maven-parent version to 1.2.0


## [v2.0.0] [r5.17.1] - 2024-04-23

- Switched JSON management to gcube-jackson [#19115]
- Switched smart-executor JSON management to gcube-jackson [#19647]
- Fixed workspace folder creation [#19056]
- Using StorageHubClient in place of Home Library Webapp HTTP calls
- Added the generation of a CSV to post analyze the calledMethods
- Fixed distro files and pom according to new release procedure
- Ported plugin to smart-executor v3.X.X [#21615]
- Ported plugin to works with TimescaleDB [#21347]
- Enhanced range of storagehub-client-library to 2.0.0,3.0.0-SNAPSHOT [#22824]


## [v1.5.0] [r4.13.0] - 2018-11-20

- Creating uber-jar instead of jar-with-dependencies [#10136]


## [v1.4.0] [r4.11.0] - 2018-04-12

- Regex Rules are loaded every time the plugin is launched [#11233]
- Added possibility to force to rerun an already completed aggregation [#11258]
- Added possibility to force to restart immediately an interrupted aggregation [#11280]
- Added possibility to force to start an aggregation even is too early to do it [#11281]
- Logging rows elaboration every 5% rows when the total number is greatest than 100000

## [v1.3.0] [r4.10.0] - 2018-02-15

- Fixed duplication of records in case of timeout retry
- Fixed issues when aggregating old StorageUsageRecords

## [v1.2.0] [r4.7.0] - 2017-10-09

- Renamed the aggregation modes [#9023]
- Separated Aggregation from Recovery [#9420]
- Supporting multiple concurrent launch [#9421] 
- Added parameters to avoid to write on buckets during backup or compaction time [#9422]
- Storing Aggregation/Recovery evolution on a management bucket in CouchBase [#9423]
- Aggregation get last aggregation day from management bucket [#9424]
- Recovery restart aggregation which didn't terminate getting the informations from management bucket [#9425]
- Added support to read original records from a bucket and to publish aggregated in a different one [#9556]
- Added possibility to specify a range for aggregation and recovery


## [v1.1.0] [r4.5.0] - 2017-06-07

- Refactored plugin


## [v1.0.1] [r4.3.0] - 2017-03-16

- Enhanced logging


## [v1.0.0] [r4.1.0] - 2016-11-07

- First Release

