package org.gcube.accounting.aggregator.recover;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.UUID;

import org.gcube.accounting.aggregator.aggregation.AggregatorBuffer;
import org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.ServiceUsageRecord;
import org.gcube.documentstore.exception.InvalidValueException;
import org.gcube.documentstore.records.AggregatedRecord;
import org.gcube.documentstore.records.Record;
import org.gcube.documentstore.records.RecordUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TestAggregationFromFile {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected static final String ID = Record.ID;
	
	static {
		/// One Record per package is enough
		RecordUtility.addRecordPackage(ServiceUsageRecord.class.getPackage());
		RecordUtility.addRecordPackage(AggregatedServiceUsageRecord.class.getPackage());
	}
	
	protected final File srcFile;
	
	protected final AggregatorBuffer aggregatorBuffer;
	
	protected int elaborated;
	protected int aggregated;
	
	public TestAggregationFromFile(File srcFile){
		this.srcFile = srcFile;
		this.elaborated = 0;
		this.aggregated = 0;
		this.aggregatorBuffer = new AggregatorBuffer();
	}
	
	protected void readFile() throws Exception {
		try {
			// Open the file that is the first // command line parameter
			FileInputStream fstream = new FileInputStream(srcFile);
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			
			String line;
			// Read File Line By Line
			while ((line = br.readLine()) != null) {
				elaborateLine(line);
				++elaborated;
			}
			
			br.close();
			in.close();
			fstream.close();
			
		} catch (Exception e) {
			logger.error("Error while elaborating file {}", srcFile.getAbsolutePath(), e);
			throw e;
		}

	}

	public void elaborate() throws Exception{
		logger.info("Going to elaborate {}", srcFile.getAbsolutePath());
		readFile();
	}
	
	protected void elaborateLine(String json) throws Exception {
		
		Record record = RecordUtility.getRecord(json);
		try {
			record.validate();
		}catch (InvalidValueException e) {
			return;
		}
		record.setId(UUID.randomUUID().toString());
		
		@SuppressWarnings("rawtypes")
		AggregatedRecord aggregatedRecord = AggregatorBuffer.getAggregatedRecord(record);
		aggregatorBuffer.aggregate(aggregatedRecord);
	}
	
}
