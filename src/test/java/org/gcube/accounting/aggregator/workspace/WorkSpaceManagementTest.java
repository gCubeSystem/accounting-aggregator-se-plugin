package org.gcube.accounting.aggregator.workspace;

import org.gcube.accounting.aggregator.ContextTest;
import org.gcube.accounting.aggregator.directory.WorkSpaceDirectoryStructure;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.junit.Test;

public class WorkSpaceManagementTest extends ContextTest {
	
	@Test
	public void testCreatedirectory() throws Exception {
		WorkSpaceManagement workSpaceManagement = WorkSpaceManagement.getInstance();
		
		FolderContainer root = workSpaceManagement.getWorkspaceRoot();
		
		for(int i=0; i<5; i++) {
			FolderContainer accountingAggregatorPlugin = workSpaceManagement.getOrCreateFolder(root, "Accounting-Aggregator-Plugin", WorkSpaceDirectoryStructure.BACKUP_FOLDER_DESCRIPTION, false);
			FolderContainer yearly = workSpaceManagement.getOrCreateFolder(accountingAggregatorPlugin, "MONTHLY", WorkSpaceDirectoryStructure.BACKUP_FOLDER_DESCRIPTION, false);
			workSpaceManagement.getOrCreateFolder(yearly, "2015", WorkSpaceDirectoryStructure.BACKUP_FOLDER_DESCRIPTION, false);
			workSpaceManagement.getOrCreateFolder(yearly, "2016", WorkSpaceDirectoryStructure.BACKUP_FOLDER_DESCRIPTION, false);
		}

	}
	
}
