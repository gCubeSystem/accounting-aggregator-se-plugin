package org.gcube.accounting.aggregator.analysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.gcube.accounting.aggregator.utility.Utility;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestRules {
	
	private static final Logger logger = LoggerFactory.getLogger(TestRules.class);
	
	public File getCalledMethodCSVDirectory() throws Exception {
		URL logbackFileURL = TestRules.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return new File(resourcesDirectory, "CalledMethods");
	}
	
	public List<File> allCSV() throws Exception {
		File calledMethodCSVDirectory = getCalledMethodCSVDirectory();
		FilenameFilter filenameFilter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".csv");
			}
		};
		
		List<File> csvFiles = Arrays.asList(calledMethodCSVDirectory.listFiles(filenameFilter));
		
		return csvFiles;
	}
	
	public void analyseCSVFile(File csvFile, Map<String,Map<String, Integer>> map) throws Exception {
		logger.info("Analisyng CSV file {}\n", csvFile.getAbsolutePath());
		try(BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
			for(String line; (line = br.readLine()) != null;) {
				String[] splittedLine = line.split(",");
				
				String serviceClass = splittedLine[0].substring(1,splittedLine[0].length()-1);
				String serviceName = splittedLine[1].substring(1,splittedLine[1].length()-1);
				String calledMethod = splittedLine[2].substring(1,splittedLine[2].length()-1);
				String countString = splittedLine[3].substring(1,splittedLine[3].length()-1);
				Integer count = Integer.valueOf(countString);
				
				String serviceClassServiceName = serviceClass + "," + serviceName;
				
				Map<String, Integer> calledMethodCountMap = map.get(serviceClassServiceName);
				if(calledMethodCountMap==null) {
					calledMethodCountMap = new TreeMap<String, Integer>();
					map.put(serviceClassServiceName, calledMethodCountMap);
				}
				
				Integer gotCount = calledMethodCountMap.get(calledMethod);
				if(gotCount==null) {
					gotCount = 0;
				}
				calledMethodCountMap.put(calledMethod, gotCount + count);
					
			}
		} catch(Exception e) {
			throw e;
		}
	}
	
	@Test
	public void createUniqueCSV() throws Exception {
		
		List<File> csvFiles = allCSV();

		Map<String,Map<String, Integer>> map = new TreeMap<>();
		
		for(File csvFile : csvFiles) {
			analyseCSVFile(csvFile, map);
		}
		
		generateCSV(map);
	}
	
	public void generateCSV(Map<String,Map<String, Integer>> map) throws Exception {
		File generatedCSV = new File("generated.csv");
		logger.info("Going to generated CSV suammary file {}\n", generatedCSV.getAbsolutePath());
		if(generatedCSV.exists()) {
			generatedCSV.delete();
		}
		
		for(String serviceClassServiceName : map.keySet()) {
			Map<String, Integer> calledMethodCountMap = map.get(serviceClassServiceName);
			for(String calledMethod : calledMethodCountMap.keySet()) {
				Integer count = calledMethodCountMap.get(calledMethod);
				Utility.printLine(generatedCSV, "\"" + serviceClassServiceName.replace(",", "\",\"") + "\",\"" + calledMethod + "\","  + String.valueOf(count.intValue()));
			}
		}
		
		logger.info("File {} successfully generated\n", generatedCSV.getAbsolutePath());
	}
	
}
