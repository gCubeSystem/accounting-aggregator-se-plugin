package org.gcube.accounting.aggregator.workspace;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.gcube.accounting.aggregator.utility.Utility;
import org.gcube.common.storagehub.client.dsl.ContainerType;
import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.ItemContainer;
import org.gcube.common.storagehub.client.dsl.ListResolver;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class WorkSpaceManagement {
	
	public static Logger logger = LoggerFactory.getLogger(WorkSpaceManagement.class);
	
	private static final String ZIP_SUFFIX = ".zip";
	private static final String ZIP_FILE_DESCRIPTION = "Backup of original records deleted and aggregtaed records inserted.";
	private static final String ZIP_MIMETYPE = "application/zip, application/octet-stream";
	
	protected StorageHubClient storageHubClient;
	
	public static WorkSpaceManagement getInstance() {
		return new WorkSpaceManagement();
	}
	
	protected WorkSpaceManagement() {
		storageHubClient = new StorageHubClient();
	}
	
	public void addToZipFile(ZipOutputStream zos, File file) throws Exception {
		
		byte[] buffer = new byte[1024];
		
		FileInputStream in = new FileInputStream(file);
		
		ZipEntry ze = new ZipEntry(file.getName());
		zos.putNextEntry(ze);
		int len;
		while((len = in.read(buffer)) > 0) {
			zos.write(buffer, 0, len);
		}
		zos.closeEntry();
		in.close();
	}
	
	private String getZipFileName(String name) throws Exception {
		String zipFileName = String.format("%s%s", name, ZIP_SUFFIX);
		return zipFileName;
	}
	
	public boolean zipAndBackupFiles(FolderContainer targetFolder, String name, List<File> files) throws Exception {
		
		try {
			String zipFileName = getZipFileName(name);
			
			File zipFile = new File(files.get(0).getParentFile(), zipFileName);
			zipFile.delete(); // Removing old zip file if any
			logger.trace("Going to save {} into workspace", zipFile.getAbsolutePath());
			
			FileOutputStream fos = new FileOutputStream(zipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);
			
			for(File file : files) {
				addToZipFile(zos, file);
			}
			
			zos.close();
			
			FileInputStream zipFileStream = new FileInputStream(zipFile);
			
			WorkSpaceManagement.getInstance().uploadFile(zipFileStream, zipFileName, ZIP_FILE_DESCRIPTION, ZIP_MIMETYPE,
					targetFolder);
			
			logger.debug("Going to delete local zip file {}", zipFile.getAbsolutePath());
			zipFile.delete();
			
			for(File file : files) {
				if(file.exists()) {
					logger.debug("Going to delete local file {} which was added to the zip file {}",
							file.getAbsolutePath(), zipFile.getAbsolutePath());
					file.delete();
				}
			}
			
			return true;
		} catch(Exception e) {
			logger.error("Error while trying to save a backup file containg aggregated records", e);
			throw e;
		}
	}
	
	public FolderContainer getWorkspaceRoot() throws Exception {
		try {
			return storageHubClient.getWSRoot();
		} catch(Exception e) {
			String username = Utility.getUsername();
			logger.info("Unable to create the Workspace Root for {}.", username);
			throw e;
			/*
			logger.info("Unable to obtain the Workspace Root for {}. Going to create it.", username);
			try {
				HomeManagerFactory factory = HomeLibrary.getHomeManagerFactory();
				HomeManager manager = factory.getHomeManager();
				User user = manager.createUser(username);
				@SuppressWarnings("deprecation")
				Home home = manager.getHome(user);
				Workspace ws = home.getWorkspace();
				ws.getRoot();
				return storageHubClient.getWSRoot();
			} catch(Exception ex) {
				logger.info("Unable to create the Workspace Root for {}.", username);
				throw e;
			}
			*/
		}
	}
	
	protected FolderContainer getFolder(FolderContainer parent, String name) throws StorageHubException {
		FolderContainer destinationFolder = null;
		
		ListResolver listResolver = parent.findByName(name);
		List<ItemContainer<? extends Item>> itemContainers = listResolver.getContainers();
		if(itemContainers.size()>=1){
			ItemContainer<? extends Item> itemContainer = itemContainers.get(0);
			if(itemContainer.getType().compareTo(ContainerType.FOLDER)==0) {
				destinationFolder = (FolderContainer) itemContainer;
			}
		}
		
		return destinationFolder;
	}
	
	public FolderContainer getOrCreateFolder(FolderContainer parent, String name, String description, boolean hidden)
			throws Exception {
		FolderContainer destinationFolder = getFolder(parent, name);
		
		/*
		ListResolverTyped listResolverTyped = parent.list();
		List<ItemContainer<? extends Item>> containers = listResolverTyped.includeHidden().getContainers();
		for(ItemContainer<? extends Item> itemContainer : containers) {
			if(itemContainer instanceof FolderContainer) {
				if(itemContainer.get().getName().compareTo(name) == 0) {
					destinationFolder = (FolderContainer) itemContainer;
				}
			}
		}
		*/
		
		if(destinationFolder == null) {
			if(hidden) {
				destinationFolder = parent.newHiddenFolder(name, description);
			} else {
				destinationFolder = parent.newFolder(name, description);
			}
		}
		return destinationFolder;
	}
	
	public FileContainer uploadFile(InputStream inputStream, String fileName, String description, String mimeType,
			FolderContainer parentPath) throws Exception {
		try {
			logger.trace("Going to upload file on WorkSpace name:{}, description:{}, mimetype:{}, parentPath:{}",
					fileName, description, mimeType, parentPath);
			FileContainer filecontainer = parentPath.uploadFile(inputStream, fileName, description);
			
			logger.info("Zip file {} successfully uploaded in workspace", fileName);
			return filecontainer;
		} catch(Exception e) {
			logger.error("Error while uploading file on WorkSpace", e);
			throw e;
		}
	}
}
