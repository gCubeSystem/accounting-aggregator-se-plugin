package org.gcube.accounting.aggregator.directory;

import org.gcube.accounting.aggregator.workspace.WorkSpaceManagement;
import org.gcube.common.storagehub.client.dsl.FolderContainer;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class WorkSpaceDirectoryStructure extends DirectoryStructure<FolderContainer>{
	
	public static final String BACKUP_FOLDER_DESCRIPTION = "Accounting Aggregator Plugin Backup Folder"; 
	
	@Override
	protected FolderContainer getRoot() throws Exception {
		return WorkSpaceManagement.getInstance().getWorkspaceRoot();
	}

	@Override
	protected FolderContainer createDirectory(FolderContainer parent, String name) throws Exception {
		return WorkSpaceManagement.getInstance().getOrCreateFolder(parent, name, BACKUP_FOLDER_DESCRIPTION, false);
	}

	
}
