package org.gcube.accounting.aggregator.aggregation;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public enum DesignID {

	accounting_storage_status("StorageStatusRecordAggregated","all"),
	StorageUsageRecord("StorageUsageRecordAggregated","all"),
	ServiceUsageRecord("ServiceUsageRecordAggregated","all"),
	AggregatedServiceUsageRecord("ServiceUsageRecordAggregated","all"),
	JobUsageRecord("JobUsageRecordAggregated","all");
	
	
	private String designName;
	private String viewName;
	
	private DesignID(String designName, String viewName) {
		this.designName = designName;
		this.viewName = viewName;
	}

	public String getDesignName() {
		return designName;
	}

	public String getViewName() {
		return viewName;
	}
	
}
