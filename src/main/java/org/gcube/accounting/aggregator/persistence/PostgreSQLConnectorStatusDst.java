package org.gcube.accounting.aggregator.persistence;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
class PostgreSQLConnectorStatusDst extends PostgreSQLConnectorStatus implements AggregatorPersistenceStatusDst {

	protected PostgreSQLConnectorStatusDst() throws Exception {
		super(AggregatorPersistenceDst.class);
	}


}
