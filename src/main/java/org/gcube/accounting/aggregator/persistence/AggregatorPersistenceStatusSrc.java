package org.gcube.accounting.aggregator.persistence;

/**
 * @author Luca Frosini (ISTI - CNR)
 * Used only to migrate status to move database.
 * Do not use for normal operation of aggregation
 */
interface AggregatorPersistenceStatusSrc extends AggregatorPersistenceStatus {

}
