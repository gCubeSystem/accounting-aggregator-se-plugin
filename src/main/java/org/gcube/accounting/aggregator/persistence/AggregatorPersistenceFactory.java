package org.gcube.accounting.aggregator.persistence;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class AggregatorPersistenceFactory {

	private static AggregatorPersistenceStatus aggregatorPersistenceStatus;
	
	private static AggregatorPersistenceStatusSrc aggregatorPersistenceStatusSrc;
	private static AggregatorPersistenceStatusDst aggregatorPersistenceStatusDst;
	
	private static AggregatorPersistenceSrc aggregatorPersistenceSrc;
	private static AggregatorPersistenceDst aggregatorPersistenceDst;
	
	public static AggregatorPersistenceStatus getAggregatorPersistenceStatus() throws Exception {
		if(aggregatorPersistenceStatus == null) {
			aggregatorPersistenceStatus = new PostgreSQLConnectorStatus();
		}
		return aggregatorPersistenceStatus;
	}
	
	protected static AggregatorPersistenceStatusSrc getAggregatorPersistenceStatusSrc() throws Exception {
		if(aggregatorPersistenceStatusSrc == null) {
			aggregatorPersistenceStatusSrc = new PostgreSQLConnectorStatusSrc();
		}
		return aggregatorPersistenceStatusSrc;
	}
	
	protected static AggregatorPersistenceStatusDst getAggregatorPersistenceStatusDst() throws Exception {
		if(aggregatorPersistenceStatusDst == null) {
			aggregatorPersistenceStatusDst = new PostgreSQLConnectorStatusDst();
		}
		return aggregatorPersistenceStatusDst;
	}
	
	public static AggregatorPersistenceSrc getAggregatorPersistenceSrc() throws Exception {
		if(aggregatorPersistenceSrc == null) {
			aggregatorPersistenceSrc = new PostgreSQLConnectorSrc();
		}
		return aggregatorPersistenceSrc;
	}
	
	public static AggregatorPersistenceDst getAggregatorPersistenceDst() throws Exception {
		if(aggregatorPersistenceDst == null) {
			aggregatorPersistenceDst = new PostgreSQLConnectorDst();
		}
		return aggregatorPersistenceDst;
	}
	
}
