package org.gcube.accounting.aggregator.persistence;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class PostgreSQLConnectorStatus extends PostgreSQLConnector implements AggregatorPersistenceStatus {

	protected PostgreSQLConnectorStatus() throws Exception {
		super(AggregatorPersistenceSrc.class);
	}

	protected PostgreSQLConnectorStatus(Class<?> clazz) throws Exception {
		super(clazz);
	}
}
