package org.gcube.accounting.aggregator.persistence;

import org.gcube.accounting.persistence.AccountingPersistenceConfiguration;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class AggregatorPersistenceConfiguration extends AccountingPersistenceConfiguration {

	/**
	 * Default Constructor
	 */
	public AggregatorPersistenceConfiguration() {
		super();
	}

	/**
	 * @param persistence The class of the persistence to instantiate
	 * @throws Exception if fails
	 */
	@SuppressWarnings({ "rawtypes" })
	public AggregatorPersistenceConfiguration(Class<?> persistence) throws Exception {
		super((Class) persistence);
	}

}
