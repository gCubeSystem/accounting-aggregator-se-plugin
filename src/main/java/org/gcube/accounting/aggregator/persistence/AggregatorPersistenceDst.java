package org.gcube.accounting.aggregator.persistence;

import org.gcube.documentstore.records.Record;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface AggregatorPersistenceDst extends AggregatorPersistence {

	public void insert(Record record) throws Exception;

	public void commitAndClose() throws Exception;

}
