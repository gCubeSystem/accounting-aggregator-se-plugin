package org.gcube.accounting.aggregator.persistence;

import java.util.Date;
import java.util.List;

import org.gcube.accounting.aggregator.aggregation.AggregationType;
import org.gcube.accounting.aggregator.status.AggregationStatus;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface AggregatorPersistenceStatus {

	public List<AggregationStatus> getUnterminated(String recordType, AggregationType aggregationType,
			Date aggregationStartDate, Date aggregationEndDate, boolean forceRestart) throws Exception;

	public List<AggregationStatus> getAll() throws Exception;

	public AggregationStatus getAggregationStatus(String recordType, AggregationType aggregationType,
			Date aggregationStartDate) throws Exception;

	public AggregationStatus getLast(String recordType, AggregationType aggregationType, Date aggregationStartDate,
			Date aggregationEndDate) throws Exception;

	public void upsertAggregationStatus(AggregationStatus aggregationStatus) throws Exception;

}
