package org.gcube.accounting.aggregator.persistence;

import java.sql.ResultSet;

import org.gcube.accounting.aggregator.status.AggregationStatus;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface AggregatorPersistenceSrc extends AggregatorPersistence {

	public ResultSet getResultSetOfRecordToBeAggregated(AggregationStatus aggregationStatus) throws Exception;
	
	public void deleteRecord(JsonNode jsonNode) throws Exception;
	
	public boolean isBulkDeleteAllowed();
	
	/**
	 * It must be implemented only and only if isBulkDeleteAllowed()
	 * return true. It must raise UnsupportedOperationException if bulk delete is not allowed
	 */
	public void deleteRecords(ArrayNode array) throws UnsupportedOperationException, Exception;

	public int getEstimatedRecordRecordToBeAggregated(AggregationStatus aggregationStatus) throws Exception;
	
}
