package org.gcube.accounting.aggregator.persistence;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class PostgreSQLConnectorSrc extends PostgreSQLConnector implements AggregatorPersistenceSrc {

	protected PostgreSQLConnectorSrc() throws Exception {
		super(AggregatorPersistenceSrc.class);
	}

}
