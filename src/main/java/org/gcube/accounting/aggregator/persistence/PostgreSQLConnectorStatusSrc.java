package org.gcube.accounting.aggregator.persistence;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
class PostgreSQLConnectorStatusSrc extends PostgreSQLConnectorStatus implements AggregatorPersistenceStatusSrc {

	protected PostgreSQLConnectorStatusSrc() throws Exception {
		super(AggregatorPersistenceSrc.class);
	}

}
