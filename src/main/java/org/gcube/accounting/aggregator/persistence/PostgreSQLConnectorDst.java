package org.gcube.accounting.aggregator.persistence;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class PostgreSQLConnectorDst extends PostgreSQLConnector implements AggregatorPersistenceDst {

	protected PostgreSQLConnectorDst() throws Exception {
		super(AggregatorPersistenceDst.class);
	}

}
